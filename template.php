<?php
function phptemplate_tinymce_theme($init, $textarea_name, $theme_name, $is_running) {
  static $access, $integrated;

  if (!isset($access)) {
    $access = function_exists('imce_access') && imce_access();
  }

  $init = theme_tinymce_theme($init, $textarea_name, $theme_name, $is_running);

  if ($init && $access) {
    $init['file_browser_callback'] = 'imceImageBrowser';
    if (!isset($integrated)) {
      $integrated = TRUE;
      drupal_add_js("
function imceImageBrowser(field_name, url, type, win) {
  tinyOpenerWin = win, tinyTargetField = field_name;
  if (typeof tinyImceWin == 'undefined' || tinyImceWin.closed) {
    tinyImceWin = window.open(Drupal.settings.basePath +'?q=imce', '', 'width=760,height=560,resizable=1');
    tinyImceWin['imceOnLoad'] = function () {
      tinyImceWin.imce.highlight(url.substr(url.lastIndexOf('/')+1));
      tinyImceWin.imce.setSendTo(Drupal.t('Send to @app', {'@app': 'TinyMCE'}), function(file) {
        window.focus();
        tinyOpenerWin.focus();
        $('#width', tinyOpenerWin.document).val(file.width);
        $('#height', tinyOpenerWin.document).val(file.height);
        $('#'+ tinyTargetField, tinyOpenerWin.document).val(file.url).focus();
      });
    }
  }
  else {
    tinyImceWin.imce.highlight(url.substr(url.lastIndexOf('/')+1));
  }
  tinyImceWin.focus();
}
", 'inline');
    }
  }

  return $init;
}
?>