<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
	<title><?php print $head_title ?></title>
	<!--[if lte IE 6]>
	<style>
	body {behavior:url("<?php print $base_path . $directory ?>/csshover.htc");}
	</style>
	<![endif]-->
    <?php print $styles ?>
	<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="<?php print $base_path . $directory ?>/ie6.css" />
	<![endif]-->
    <!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="<?php print $base_path . $directory ?>/ie7.css" />
	<![endif]-->
    <?php print $scripts ?>
</head>

<body class="<?php print $body_classes ?>">
<div id="page_wrapper">
	<div id="header">
		<div id="header_blocks"><?php print $header_banner ?></div>
		<?php if($logo): ?><a href="<?php print $frontpage ?>"><img src="<?php print $logo ?>" /></a><?php endif ?><?php if($site_name): ?><h1 class="page-title"><?php print $site_name ?></h1><?php endif ?><?php if($site_slogan): ?><span class="page-slogan"><?php print $site_slogan ?></span><?php endif ?>
	</div>
	<div id="additional_header"><?php print $additional_header ?></div>
	<div id="columns">
		<div id="content_bar">
			<?php print $breadcrumb; ?>
            <?php if ($show_messages && $messages): print $messages; endif; ?>
			<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
			<div id="top_content"><?php print $top_content ?></div>
			<?php print $help; ?>
			<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
			<?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
			<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
			<?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
			<div id="main_content"><?php print $content ?></div>
			<div id="bottom_content"><?php print $bottom_content ?></div>
		</div>
	</div>
	<div id="left_bar"><?php print $left_bar ?><div id="credits">Design by <a href="http://www.tibiafusion.com" Title="Comunidad Chilena de Tibia"> TibiaFusion</a></div></div>
	<div id="right_bar"><?php if($search_box): ?><div id="theme-search"><?php print $search_box ?></div><?php endif ?><?php print $right_bar ?></div>
	<div id="footer">
			<div id="footer_blocks"><?php print $footer ?></div>
			<div id="footer_message"><?php print $footer_message ?></div>
			<?php print $closure ?>
	</div>
</div>
</body>
</html>